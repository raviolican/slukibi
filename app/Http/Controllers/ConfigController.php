<?php

namespace App\Http\Controllers;
use App\Inventory;
use Illuminate\Contracts\Session\Session;
Use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Cache;

class ConfigController extends Controller
{
    public function index(Request $request, $uuid)
    {
        $cfg = \App\Setting::with('owner')->where('location', $uuid)->get();
        return view('config.index', [ 'uuid'=> $uuid, 'cfg' => $cfg]);
    }

    public function verify(Request $request, $uuid)
    {
       //dd($request->password);
         if(strlen($request->password) <= 0) 
            return redirect()->back()->withErrors("Password cannot be empty. 
            Copy and paste it from your chat window of your viewer.");
         
        $cfg = \App\Setting::where([
            'location' => $uuid,
            'secret_url_pass' => $request->password
        ])->first();
        if($cfg != NULL) {
            Auth::login($cfg->owner()->first(), false);
            return redirect()->back();
        } else {
            return redirect()->back()->withErrors("Copy and paste the password from your chatwindow.");
        }
    }

    public function general($uuid)
    {
        $cfg = \App\Setting::with('owner')->where('location', $uuid)->get();
        return view('config.general', ['uuid' => $uuid, 'cfg' => $cfg, 'huntobjects' => json_decode($cfg[0]->huntobjects,true)]);
    }

    public function savesettings(Request $request, $uuid)
    {
        $this->validate($request, [
            'terms_msg' => 'required',
            'welcome_msg' => 'required',
            "a_p" => "required|numeric",
            "a_w" => "required|numeric",
            "a_d" => "required|numeric",

            "b_p" => "required|numeric",
            "b_w" => "required|numeric",
            "b_d" => "required|numeric",

            "c_p" => "required|numeric",
            "c_w" => "required|numeric",
            "c_d" => "required|numeric",

            "d_p" => "required|numeric",
            "d_w" => "required|numeric",
            "d_d" => "required|numeric",

            "e_p" => "required|numeric",
            "e_w" => "required|numeric",
            "e_d" => "required|numeric",

            "f_p" => "required|numeric",
            "f_w" => "required|numeric",
            "f_d" => "required|numeric",
        ]);

        $jVal = json_encode(array(
            "A1" => (array(
                $request->a_p, $request->a_w, $request->a_d
            )),
            "B2" => (array(
                $request->b_p, $request->b_w, $request->b_d
            )),
            "C3" => (array(
                $request->c_p, $request->c_w, $request->c_d
            )),
            "D4" => (array(
                $request->d_p, $request->d_w, $request->d_d
            )),
            "E5" => (array(
                $request->e_p, $request->e_w, $request->e_d
            )),
            "F6" => (array(
                $request->f_p, $request->f_w, $request->f_d
            )),
        ));
        $modReq = $request->except("_token");

        if(isset($modReq["antibot"]) && $modReq["antibot"] == "on") {
            $modReq["antibot"] = 1;
        }
        elseif(isset($modReq["antibot"]) && $modReq["antibot"] != "on"){
            $request->session()->flash('error', 'Invalid inputs. Please try again.');
            return redirect()->back();
        } else {
            $modReq["antibot"] = 0;
        }

        if(Auth::user()->setting()->where('location', $uuid)->update([
            "terms_msg"=> $request->terms_msg,
            "welcome_msg" => $request->welcome_msg,
            "antibot"=> $modReq["antibot"],
            "huntobjects" => $jVal
        ])){
            // Update Cache...
            Cache::remember("svrcfg|".Auth::user()->uuid."|".$uuid, 60, function() use ($uuid)
            {
                return  Auth::user()->setting()->where('location', $uuid)->get();
            });

            $request->session()->flash('success', 'General settings saved! Updating takes up to 1 minute.');
        }else{
            $request->session()->flash('error', 'General database error!');
        }
        return redirect()->back();
    }

    public function products(Request $request, $uuid)
    {
        $up = Auth::user()->with(array("inventory" => function($query) use($uuid) {
                $query->where('location', $uuid);
            }, "setting" => function($query) use($uuid) {
            $query->where('location', $uuid);
            }, "product" => function($query) use($uuid) {
            $query->where('location', $uuid);
        }))->get();
        $jsonVar = json_decode($up[0]->setting[0]->products);
        $cfg = \App\Setting::with('owner')->where('location', $uuid)->get();

        return view('config.products', [
            'uuid' => $uuid,
            'products' => $up,
            'configured_products' => $up[0]->product,
            "inventory" => $up[0]->inventory
        ]);
    }

    public function createproduct(Request $request, $uuid)
    {
        $this->validate($request, [
            'object' => 'required',
            'texture' => 'required',
            'points' => 'required|numeric',
          #  'name'  => 'required'
        ]);

        if($request->notecard == -1)
            $myVal = "none";
        else
            $myVal = Auth::user()->inventory->find($request->notecard)->name;

       # dd(Auth::user()->inventory->find($request->object));
        $product = new \App\Product(array(
            'owner_uuid' => Auth::user()->uuid,
            'location' => $uuid,
            'object' => Auth::user()->inventory->find($request->object)->name,
            'texture' => Auth::user()->inventory->find($request->texture)->uuid,
            'notecard' => $myVal ,
            'price' => $request->points

        ));
       # dd($product);
        if(Auth::user()->product()->save($product)) {
            $request->session()->flash("success", "New product saved successfully!!");
        };

        return redirect()->back();
    }

    public function delete_product($uuid,$id)
    {
        Auth::user()->product()->find($id)->forceDelete();
    }

    public function logs($uuid) {
        $myLog = Auth::user()->setting()->first()->log()->orderBy('created_at', 'desc')->get();

        return view("config.log", [ 'uuid'=> $uuid, 'log' => $myLog]);


    }

    public function logout()
    {
        Auth::logout(false);
        return redirect()->back()->withErrors("You have been logged out!");
    }
}