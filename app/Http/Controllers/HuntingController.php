<?php

namespace App\Http\Controllers;

use App\Log;
use App\Owner;
use App\Player;
use App\Setting;
use App\User;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class HuntingController extends Controller
{
    public function fetch_servers(Request $request)
    {
        $owner = Owner::where('uuid', $request->uuid    );
        $svr = $owner->first()->setting()->get();
        $iVar = count($svr);
        $j = 0;
        foreach($svr as $key=>$val)
        {
            $j++;
            if($j == $iVar)
                echo $val->id;
            else
                echo $val->id.",";
        }
    }

    public function fetch_configs(Request $request)
    {
        /* no Caching!
        $owner = Owner::where('uuid', $request->uuid    );
        $settings = $owner->first()->setting()->find($request->settings_id);
        */
        // Caching
        $settings = Cache::remember("svrcfg|".$request->uuid."|".$request->settings_id, 60, function() use ($request)
        {
            $owner = Owner::where('uuid', $request->uuid    );
            return $owner->first()->setting()->find($request->settings_id);
        });

        if($settings !== NULL){
            $settings = $settings->toArray();
            $j = json_decode($settings["huntobjects"],true)[$request->id];
            $settings["huntobjects"] = implode(",",$j);
        } else {
            return 0;
        }
        return $settings;
    }

    public function check_user(Request $request)
    {
        $player = Player::where('uuid',$request->uuid)->first();
      #  dd($player);
        if($player == NULL) // User exiusts?
        { // NO so...
            // Create new User!
            $player = new Player($request->all());
            Setting::where('id',$request->settings_id)->first()->player()->save($player);
            return "user_created";
        } else { // yes does exist
            return $player->first()->points;
        }
    }

    public function collect(Request $request)
    {
        $player = Player::where('uuid', $request->uuid)->with("setting");
        $cfgpoints = json_decode($player->first()->setting->huntobjects, true);

        $oldpoints = $player->first()->points;

        $newpoints = round($oldpoints,2) + round($cfgpoints[$request->id][0],2);

        Player::where('uuid', $request->uuid)->with("setting")->update(
            ["points" => $newpoints]
        );

        $mo = new Log(
            [
                "ev_name" => "Point Transaction",
                "ev_str" => round($cfgpoints[$request->id][0],2)." → ".$player->first()->username,
                "settings_id" => $player->first()->settings_id
            ]
        );
        $mo->save();

        echo "Transaction ID: #$mo->id +". $cfgpoints[$request->id][0]."p → $newpoints points";
    }
    public function getproducts(Request $request) {
        $owner = Owner::where('uuid', $request->uuid );
        $products = $owner->first()->product()->where("location",
            $owner->first()->setting()->find($request->settings_id)->location
            )->get([
            "texture", "price", "id"
        ]);

        if(count($products) != 0)
        {
            return(json_encode($products));
        } else {
            echo "NO_PRODUCTS";
        }
    }
    /* Sends an e-Mail! */
    public function notecard(Request $request)
    {
        $str = "";
        $owner = Owner::where('uuid', $request->uuid );
        $header = 'From: ljaksdf765@example.com' . "\r\n" .
            'Reply-To: webmaster@example.com' . "\r\n" .
            'X-Mailer: PHP/' . phpversion();

        $product = $owner->first()->product()->find($request->id);
        // dd($product);
        if($product != NULL) {
            $loc = $owner->first()->setting()->where("id", $request->server_id)->first();


            if(substr($product->notecard,-5) == ".note") // should be == {{
            {
                $mo = new Log(
                    [
                        "ev_name" => "Deliver Notecard",
                        "ev_str" => $product->notecard." → ".$request->username."",
                        "settings_id" => $request->server_id
                    ]
                );
                $mo->save();

                mail($loc->location.'@lsl.secondlife.com', 'send_notecard', $request->target.","
                    .substr($product->notecard,0,-5).",", $header);
            }
            else{
                $mo = new Log(
                    [
                        "ev_name" => "Deliver Notecard",
                        "ev_str" => "NONE → ".$request->username." E: NO_NOTECARD",
                        "settings_id" => $request->server_id
                    ]
                );
                $mo->save();

                return "no_nc";
            }
        } else {
            $mo = new Log(
                [
                    "ev_name" => "Deliver Notecard",
                    "ev_str" => "NONE → ".$request->username." E: NO_NOTECARD",
                    "settings_id" => $request->server_id
                ]
            );
            $mo->save();

            return "no_nc";


        }



    }

    public function deliver_product(Request $request){
        $player = Player::where("uuid", $request->target);
        $owner = Owner::where('uuid', $request->uuid );
        $product = $owner->first()->product()->find($request->id);
        $loc = $owner->first()->setting()->where("id", $request->server_id)->first();

        if($player->first()->points < $product->price) {
            return "You have too less points, please earn more. You have: ".$player->first()->points.
                " needed are: ". $product->price;
        } else {
            // Enought points!

            // A: Update points
            $oldpoints = $player->first()->points;
            $newpoints = round($oldpoints,2) - round($product->price,2);
            $player->with("setting")->update(
                ["points" => $newpoints]
            );
            if(substr($product->object,-4) == ".obj") {
                // B Deliver Product
                $header = 'From: ljaksdf765@example.com' . "\r\n" .
                    'Reply-To: webmaster@example.com' . "\r\n" .
                    'X-Mailer: PHP/' . phpversion();

                mail($loc->location.'@lsl.secondlife.com', 'send_product', $player->first()->uuid.","
                    .substr($product->object,0,-4).",", $header);

                $mo = new Log(
                    [
                        "ev_name" => "Deliver Product",
                        "ev_str" => $product->object." → ".$request->username." P: ".$product->price,
                        "settings_id" => $request->server_id
                    ]
                );
                $mo->save();
                return "Your order was received and you will receive your product within a minute.";

            } else {
                $mo = new Log(
                    [
                        "ev_name" => "Deliver Product",
                        "ev_str" => "NONE → ".$request->username." E: INVALID_OBJECT",
                        "settings_id" => $request->server_id
                    ]
                );
                $mo->save();
                return "INVALID_OBJECT";
            }

        }
    }
}
