<?php

namespace App\Http\Controllers; 
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use \App\Owner;
use phpDocumentor\Reflection\Types\Null_;

class OwnerController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    public function register(Request $request) 
    {    
        /* Cache that settings! */
        $value = Cache::remember('servercfg', 9999999, function () {
            return DB::table('servercfg')->get();
        });

        $owner = new Owner();
        $owner->username   = $request->username;
        $owner->uuid       = $request->uuid;
        if(Owner::where('uuid', $request->uuid)->first() !== NULL)
        {
            echo $value[0]->already_member;
        } else {
            try {
                $owner->save();
            } catch (Exception $ex) {
                echo $value[0]->server_error;
            }
            echo $value[0]->welcome;
        }
        
        $this->register_location($request);
    }
    private function register_location(Request $request)
    {
        $value = Cache::remember('servercfg', 99999, function () {
            return DB::table('servercfg')->get();
        });
        
        $owner = Owner::where("uuid", $request->uuid)->first();
        
        $res = $owner->setting->where('location', $request->location)->first();

        if($res == NULL) {
            $config = new \App\Setting(array(
                "location"      => $request->location,
                "owner_uuid"    => $request->uuid,
            ));
            $owner->setting()->save($config);
        } else {
            return $value[0]->old_server;
        }
    }
    
    public function make_url(Request $request) 
    {
        $pass = $this->generateRandomString(6);
        $owner = \App\Owner::where('uuid', $request->uuid)->first();
        #dd($owner);
        $q = $owner->setting()->where('location', $request->location);

        /* Todo: Fix error*/
        if($q->first() == NULL){
        } else {
            $q->update([
                "secret_url_pass" => $pass
            ]);
        }

        return $pass;
    }
    public function save_products(Request $request) {
        $myProducts = json_decode($request->objects);
        $owner = \App\Owner::where('uuid', $request->uuid)->first();

        $owner->inventory()->where('location', $request->object_uuid)->forceDelete();

        foreach ($myProducts as $product) {
            $u = explode(',',$product,3);
           # dd($u[1]);
            $t = new \App\Inventory(array(
                        'name'      => $u[0],
                        'perms'     => $u[1],
                        'uuid'      => $u[2],
                        'location'  => $request->object_uuid,
                        'owner_uuid'=> $request->uuid             
                    ));

            $owner->inventory()->save($t);
        }
    }
    private function generateRandomString($length = 16) {
        return substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length/strlen($x)) )),1,$length);
    }
}
