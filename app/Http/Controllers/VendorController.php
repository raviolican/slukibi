<?php

namespace App\Http\Controllers;

use App\Setting;
use Illuminate\Http\Request;

class VendorController extends Controller
{
    /*
     *
     */
    public function get(Request $request) {
        $settings = Setting::find($request->server_id);

        $settings->owner()->products()->where('location', $settings->location)->get();

    }

    public function deliver() {

    }
}
