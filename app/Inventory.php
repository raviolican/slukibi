<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Inventory extends Model
{
    protected $fillable= [
        'name', 'location', 'price', 'owner_uuid', 'uuid', 'perms'
    ];

    public function user()
    {
        return $this->belongsTo("App\User");
    }
    public function owner() {
        return $this->hasOne("App\Owner", 'uuid', 'owner_uuid');
    }
}
