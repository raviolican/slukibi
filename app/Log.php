<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    protected $fillable = [
        "ev_str", "ev_name", "settings_id"
    ];
    public function setting()
    {
        return $this->belongsTo("App\Setting", "id", "settings_id");
    }
}
