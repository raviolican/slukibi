<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace App;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Owner extends Authenticatable {
    use Notifiable;
    
    protected $fillable = [
        "username", "lastname", "uuid"
    ];
    
    public function players() 
    {
        return $this->hasMany("Player");
    }
    
    public function product() 
    {
        return $this->hasMany(Product::class, 'owner_uuid', 'uuid');  
    }
    
    public function setting() 
    {
        return $this->hasMany(Setting::class, 'owner_uuid', 'uuid');
    }

    public function inventory()
    {
        return $this->hasMany(Inventory::class, 'owner_uuid', 'uuid');
    }
    
}