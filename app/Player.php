<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace App;
use Illuminate\Database\Eloquent\Model;

class Player extends Model 
{
    protected $fillable = [
        "uuid", "username", "settings_id", "points"
    ];
    
    public function owner() 
    {
        $this->hasOne("App\Owner");
    }
    public function setting() {
        return $this->hasOne("App\Setting", "id", "settings_id");
    }
}