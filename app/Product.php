<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable= [
        'name', 'location', 'price', 'owner_uuid',  'object', 'texture', 'notecard'
    ];
    public function user() 
    {
        return $this->belongsTo("App\User");
    }
    

}