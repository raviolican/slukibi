<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $fillable = [
        "location", "owner_uuid", "terms_msg", "welcome_msg", "antibot"
    ];
    public function owner() {
        return $this->hasOne("App\Owner", 'uuid', 'owner_uuid');
    }
    public function player() {
        return $this->hasMany("App\Player", "id", "settings_id");
    }
    public function log() {
        return $this->hasMany("App\Log", "settings_id", "id");
    }


}
