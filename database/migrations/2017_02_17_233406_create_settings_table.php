<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('owner_uuid', 36)->references('uuid')->on('owners');
            $table->string("configs", 255)->default("[]");
            
            $table->string("location", 36)->default("0")->unique();
            
            $table->string("secret_url_pass", 6)->default("0");
            
            $table->boolean('antibot')->default(1);
            $table->string('products')->default("[]");
            
            $table->string('welcome_msg')->default('Thanks for playing! Enjoy this great hunting game! Powered by FocusPROJECT.');
            $table->string('terms_msg')->default('You are not allowed to use bots. You are not allowed to seel free items if they are transferable. By playing you accept that we send you news about our latest product realeases');
            $table->string('latest_news')->default('03.29.1928\nNEWS SKINS ONLINE: Visit our store TODAY:D');

            $table->string("huntobjects")->default(json_encode(array(
                "A1" => (array(
                   '0.1', '15', 0.1*15*60
                )),
                "B2" => (array(
                   '0.2', '25', 0.2*25*60
                )),
                "C3" => (array(
                   '0.5', '40', 0.5*40*60
                )),
                "D4" => (array(
                   '0.7', '60', 0.7*60*60
                )),
                "E5" => (array(
                   '1', '85', 85*60
                )),
                "F6" => (array(
                   '2', '150', 2*150*60
                )),
            )));

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
