<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServercfgTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('servercfg', function (Blueprint $table) {
            $table->string("welcome")->default('Welcome to FocusPRJECT. We want to congratulate you for your new product!');
            $table->string("already_member")->default("Welcome back!");
            $table->string("new_server")->default("New server has been registered!");
            $table->string("old_server")->default("Server already registered.");
            $table->string("server_error")->default("Error. Please contact support!");
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('servercfg');
    }
}
