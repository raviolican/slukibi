<?php/*
    config.index!
*/?>

@extends('layouts.config')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">

                    <div class="panel-heading">Configuration <small> UUID: {{$uuid}} | Owner: {{$cfg[0]->owner->username}}</small></div>
                    <div class="panel-body">

                        @if(!Auth::check())
                            Error. This error has been logged.
                        @else
                            <h2>General</h2>
                            <p>
                                Manage configurations of your game.
                            </p>
                            {!! Form::open() !!}

                                {!! Form::token() !!}

                                <h3>{!! Form::label('terms', 'Terms') !!}</h3>
                                <p>Provide terms for players. They must accept in order to play. <i>When you want a new line, use '\n'.</i></p>
                                {!! Form::textarea('terms_msg',$cfg[0]->terms_msg,['cols' => '90', 'rows' => '5', 'required' => 'true']) !!}
                                <br>

                                <h3>{!! Form::label('welcome', 'Welcome Message') !!}</h3>
                                <p>When they start playing your game and register, you can set a custom welcome message.<i>When you want a new line, use '\n'.</i> </p>
                                {!! Form::text('welcome_msg',$cfg[0]->welcome_msg,[ 'required' => 'true', 'size'=>'90']) !!}

                                <h3>{!! Form::label('antibot', 'Anti-Bot Feature') !!}</h3>
                                <p>We will require the player to answer a Anti-Bot question on our website after random period of time. We can guarantee then, that all players
                                are real players! We will display small advertisements on this website.</p>
                                {!! Form::label('antibot', 'Enable Anti-Bot feature') !!}
                                {!! Form::checkbox('antibot',NULL, $cfg[0]->antibot) !!}

                                <h3>{!! Form::label('antibot', 'Hunt Object Points') !!}</h3>
                                <p>
                                    Configure points & timings for your hunting object that work with this server!
                                    Available Objects A to F (6 Objects) If you need more, please contact eder112t
                                    for an upgrade. Default settings are very experimental. We need to collect much more
                                    data in order to provide you with fair default values for both: you and the player. Use
                                    your statistics page to fine-tune settings.
                                </p>
                                <table class="display" id="idtable" >
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Points</th>
                                            <th>Waittime</th>
                                            <th>Time Disabled</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>A</td>
                                            <td><input id="row-1-age" name="a_p" value="{{$huntobjects["A1"][0]}}" type="number"  step="0.01" required></td>
                                            <td><input id="row-1-age" name="a_w" value="{{$huntobjects["A1"][1]}}" type="number"  step="1" required></td>
                                            <td><input id="row-1-age" name="a_d" value="{{$huntobjects["A1"][2]}}" type="number" step="1"required></td>
                                        </tr>
                                        <tr>
                                            <td>B</td>
                                            <td><input id="row-1-age" name="b_p" value="{{$huntobjects["B2"][0]}}" type="number"  step="0.01" required></td>
                                            <td><input id="row-1-age" name="b_w" value="{{$huntobjects["B2"][1]}}" type="number"  step="1" required></td>
                                            <td><input id="row-1-age" name="b_d" value="{{$huntobjects["B2"][2]}}" type="number"  step="1" required></td>
                                        </tr>
                                        <tr>
                                            <td>C</td>
                                            <td><input id="row-1-age" name="c_p" value="{{$huntobjects["C3"][0]}}" type="number" step="0.01" required></td>
                                            <td><input id="row-1-age" name="c_w" value="{{$huntobjects["C3"][1]}}" type="number" step="1" required></td>
                                            <td><input id="row-1-age" name="c_d" value="{{$huntobjects["C3"][2]}}" type="number" step="1" required></td>

                                        </tr>
                                        <tr>
                                            <td>D</td>
                                            <td><input id="row-1-age" name="d_p" value="{{$huntobjects["D4"][0]}}" type="number"  step="0.01" required></td>
                                            <td><input id="row-1-age" name="d_w" value="{{$huntobjects["D4"][1]}}" type="number" step="1" required></td>
                                            <td><input id="row-1-age" name="d_d" value="{{$huntobjects["D4"][2]}}" type="number" step="1" required></td>
                                        </tr>
                                        <tr>
                                            <td>E</td>
                                            <td><input id="row-1-age" name="e_p" value="{{$huntobjects["E5"][0]}}" type="number"  step="0.01" required></td>
                                            <td><input id="row-1-age" name="e_w" value="{{$huntobjects["E5"][1]}}" type="number" step="1" required></td>
                                            <td><input id="row-1-age" name="e_d" value="{{$huntobjects["E5"][2]}}" type="number" step="1" required></td>
                                        </tr>
                                        <tr>
                                            <td>F</td>
                                            <td><input id="row-1-age" name="f_p" value="{{$huntobjects["F6"][0]}}" type="number"  step="0.01" required></td>
                                            <td><input id="row-1-age" name="f_w" value="{{$huntobjects["F6"][1]}}" type="number" step="1"required></td>
                                            <td><input id="row-1-age" name="f_d" value="{{$huntobjects["F6"][2]}}" type="number" step="1" required></td>
                                        </tr>

                                    </tbody>
                                </table>
                                <br>



                                {!! Form::submit('Save',[ 'class' => "btn btn-primary" ]) !!}
                                @if(Session::has("success"))
                                <b id="notifications" style="float: right;margin-right: ;right: 10px; color:green;">
                                    {{Session::get('success')}}
                                </b>

                            @elseif(Session::has("error"))
                                <b id="notifications" style="float: right;margin-right: ;right: 10px; color:red;">
                                    {{Session::get('error')}}
                                </b>
                            @endif
                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            {!! Form::close() !!}
                        @endif

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section("after")
    <script src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
    <script>
        $(document).ready(function() {
            var table = $('#idtable').DataTable({
                "searching": false,
                "ordering": false,
                "paging": false,
                "info":     false
            });

            $('button').click(function () {
                var data = table.$('input, select').serialize();
                alert(
                    "The following data would have been submitted to the server: \n\n" +
                    data.substr(0, 120) + '...'
                );
                return false;
            });
        });
    </script>
    @if(Session::has("success"))
        <script>


            $(document).ready(function() {
                $( '#notifications' ).fadeOut( 2000 );
            });
        </script>
    @endif
 @endsection