<?php/*
    config.index!
*/?>
@extends('layouts.config')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
               
                <div class="panel-heading">Configuration <small> UUID: {{$uuid}} | Owner: {{$cfg[0]->owner->username}}</small></div>
                <div class="panel-body">
                    
                    @if(!Auth::check())
                    Welcome, here you can configure  almost all features of the game. But before that, I need to verify that you're
                    really {{$cfg[0]->owner->username}}. So please enter the password what has been sent to you.
                    <br><br>
                    
                        
                    {!! Form::open(array('url' => 'config/'.$uuid.'/login')) !!}
                    <center>
                        {!!Form::token()!!}
                   {!!  Form::text('password')!!}
                   {!!Form::submit('Verify')!!}
                    </center>
                    {{ Form::close() }}
                    @if($errors->any()) 
                    <p class="bg-warning">{{$errors->first()}}</p> 
                    @endif
                    
                    
                    @else
                    Thanks for your login! Please use the nevigation at the top
                    to navigate and perform configurations.
                    @endif
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
