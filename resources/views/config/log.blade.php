<?php/*
    config.index!
*/?>
@extends('layouts.config')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">

                    <div class="panel-heading">Configuration <small> UUID: {{$uuid}} | Owner: {{Auth::user()->username}}</small></div>
                    <div class="panel-body">
                        <h2>Logs</h2>
                        <p>
                            Here you check everything: collected points, items sent, registrations and so forth
                        </p>
                        <table class="display" id="logtable" >
                            <thead>
                            <tr>
                                <th>#ID</th>
                                <th>Event</th>
                                <th>Value</th>
                                <th>Date & Time</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($log as $key => $value)
                                <tr>
                                    <td>{{str_pad( $value->id, 8, '0', STR_PAD_LEFT)}}</td>
                                    <td>{{$value->ev_name}}</td>
                                    <td>{{$value->ev_str}}</td>
                                    <td>{{$value->created_at}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section("after")
    <script src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>

    <script>
        $(document).ready(function() {
            $('#logtable').DataTable();
        } );
    </script>

@endsection
