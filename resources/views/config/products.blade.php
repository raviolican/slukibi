<?php/*
    config.index!
*/?>
@extends('layouts.config')

@section('content')
<div class="container">
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default">
            <div class="panel-heading">Configuration <small> UUID: {{$uuid}} |
                    Owner: {{$products[0]->username}}</small></div>
            <div class="panel-body">
                @if(!Auth::check())
                    Error. This error has been logged.
                @else
                    <h2>Products</h2>
                    <p>
                        Manage your Products. To remove or add a product put/delete it
                        inside the server and click 'Sync'.
                    </p>
                    <h3>Products</h3>
                    @if(count($configured_products) <= 0)
                        <p>
                            No products created yet.
                        </p>
                    @else
                        <table class="table" id="products">
                            <tr>
                                <th>Name</th>
                                <th>Texture UUID</th>
                                <th>Notecard</th>
                                <th>Price</th>
                                <th>Options</th>
                            </tr>
                            @foreach($configured_products as $key => $value)
                                <tr>
                                    <td>{{$value->object}}</td>
                                    <td>{{substr($value->texture,0,8)}}...</td>
                                    <td>{{$value->notecard}}</td>
                                    <td>{{$value->price}}</td>
                                    <td>
                                        <button type="button" value="{{$value->id}}"
                                                class="btn btn-danger btn-xs active">
                                            <span>Delete?</span>
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    @endif
                    <button type="button" class="btn btn-primary " data-toggle="modal" data-target="#myModal">
                       Add Product
                    </button>
                    <h3>Inventory</h3>
                    <p>
                        These are all the inventory items found inside the server after the last synchronisation.
                    </p>
                    <table class="table"  >
                        <tr>
                            <th>Name</th>
                        </tr>
                        @foreach($inventory as $key => $value)
                        <tr>
                            <td>{{$value->name}}</td>
                        </tr>
                        @endforeach
                    </table>
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                @endif

                <!-- Modal -->
                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel">
                                    Create Product
                                </h4>
                            </div>
                            <div class="modal-body">
                                <p>
                                    Create a new Product. Choose Texture and a notecard and ofcourse
                                    the amount of points it costs.
                                </p>
                                {!! Form::open(array('method' => 'put')) !!}

                                    {!! Form::token() !!}
                                    <h3>{!! Form::label('object', 'Inventory Object') !!}</h3>
                                    <select class="form-control" name="object">
                                        @foreach($inventory as $item)
                                            <option value="{{$item->id}}">{{$item->name}}</option>
                                        @endforeach
                                    </select>

                                    <h3>{!! Form::label('texture', 'Texture') !!}</h3>
                                    <select class="form-control" name="texture">
                                        @foreach($inventory as $item)
                                            <option value="{{$item->id}}">{{$item->name}}</option>
                                        @endforeach
                                    </select>

                                    <h3>{!! Form::label('notecard', 'Notecard') !!}</h3>
                                    <select class="form-control" name="notecard">
                                        @foreach($inventory as $item)
                                            <option value="{{$item->id}}">{{$item->name}}</option>
                                        @endforeach
                                            <option value="-1">None</option>
                                    </select>
                                    <h3>{!! Form::label('points', 'Points Price') !!}</h3>
                                    {!!  Form::number('points', 'value') !!}<br><br>
                                {!! Form::submit('Save',[ 'class' => "btn btn-primary" ]) !!}
                                {!! Form::close() !!}

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection
@section("after")
        <script>
            $( '#products button' ).click(function(e) {
                mySel = $(this);
                $.ajaxSetup({ headers: { 'X-CSRF-TOKEN' : '{{ csrf_token() }}' } });
                $.ajax({
                    type: "DELETE",
                    url: "products/"+$(this).val(),
                    success: function() {

                        $(mySel).children("span").text("Success!");
                        $(mySel).parent().parent().remove();
                }
                });
            });
        </script>
@endsection