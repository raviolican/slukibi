<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <link href="https://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css" rel="stylesheet">

    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
</head>
<body>
    <div id="app" style='margin-top:10px;'>
         <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <h1>FocusPROJECT</h1>
                    @if(Auth::check())
                    <p>
                        <a href="general">General Configuration</a> |
                        <a href="products">Products</a> |
                        <a href="#">Statistics</a> | 
                        <a href="logs">Logs</a>
                        <a href="{{route("signout")}}" style="float: right;">LOGOUT</a>
                    </p>
                    @endif
                </div>
            </div>
        </div>
        @yield('content')
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <center>
                    (c) 2017 Focus Project. All rights reserved.
                    </center>   
                </div>
            </div>
        </div>
    </div>
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    @yield("after")
</body>
</html>
