<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::group(['prefix' => 'slapi'], function () {
    
    Route::post('register', 'OwnerController@register');
    
    Route::post('makeurl', 'OwnerController@make_url');
    
    Route::group(['prefix' => 'products'], function () {
         Route::post('save', 'OwnerController@save_products');
    });

    Route::group(['prefix' => 'hunt'], function()
    {
        Route::post('fetchsvr', 'HuntingController@fetch_servers');
        Route::post('getconfigs', 'HuntingController@fetch_configs');
        Route::post('checkusr', 'HuntingController@check_user');
        Route::post('collect', 'HuntingController@collect');

        Route::post('getproducts', 'HuntingController@getproducts');
        Route::post('delivernc', 'HuntingController@notecard');
        Route::post('deliverproduct', 'HuntingController@deliver_product');

    });
/*
    Route::group(['prefix' => 'vend'], function()
    {
        Route::post('get', 'VendorController@get');
        Route::post('deliver', 'VendorController@deliver');
    });

*/


});

Route::group(['prefix' => 'config'], function ()
{
    Route::get('{url}', 'ConfigController@index');
    Route::post('{url}/login', 'ConfigController@verify');

    Route::get('{url}/general', 'ConfigController@general');
    Route::post('{url}/general', 'ConfigController@savesettings');

    Route::get('{url}/products', 'ConfigController@products');
    Route::put('{url}/products', 'ConfigController@createproduct');
    Route::delete('{url}/products/{id}', 'ConfigController@delete_product');

    Route::post('{url}/stats', 'ConfigController@index');


    Route::get('{url}/logs', 'ConfigController@logs');

    Route::get('', 'ConfigController@logout')->name("signout");
});





Auth::routes();

Route::get('/home', 'HomeController@index');
